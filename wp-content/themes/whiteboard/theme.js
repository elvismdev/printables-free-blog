$(document).ready( function() {

  $('input, textarea').live( 'focus', function(){
    if ( $(this).attr('default_value') )
    {
      defaultValue = $(this).attr('default_value');
      if ( $(this).val() == defaultValue )
      {
        $(this).val('');
      }
    }
  });

  $('input, textarea').live( 'blur', function(){
    if ( $(this).attr('default_value') )
    {
      defaultValue = $(this).attr('default_value');
      if ( $(this).val() == '' )
      {
        $(this).val( defaultValue );
      }
    }
  });


  $('#newsletter_button_fixed').click( function() {
    if ( $('#newsletter_box_fixed').css( 'display' ) == 'none' )
    {
      $('#newsletter_box_fixed').css( 'display', 'block' );
      $('#newsletter_box_fixed').animate({
        right: '28px'
      });
    }
    else
    {
      $('#newsletter_box_fixed').animate({
        right: '-320px'
      }, function(){
        $('#newsletter_box_fixed').css( 'display', 'none' );
      });
    }
  });


});