<?php get_header(); ?>
	<div id="content">
		<?php if ( ! dynamic_sidebar( 'Alert' ) ) : ?>
			<!--Wigitized 'Alert' for the home page -->
		<?php endif ?>

<? /* Blog 728 Header */ /*<script type="text/javascript"><!--
google_ad_client = "ca-pub-7784016101949285";

google_ad_slot = "7318340421";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
*/ ?>

		<?php
		  $iCount = 0;
		  if (have_posts()) : while (have_posts()):
		    the_post();
		    $iCount++;
		    if ( $iCount == 2 ):
		?>
<!-- middle ad -->
<?php  endif;?>
			<div class="post-single">
				<h1><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				<p class="post_time_author"><?php _e('Posted on '); the_time('F j, Y'); _e(' at '); the_time(); // _e(', by '); the_author_posts_link(); ?></p>
				<?php if ( has_post_thumbnail() ) { /* loades the post's featured thumbnail, requires Wordpress 3.0+ */ echo '<div class="featured-thumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
				<div class="post-content">
					<?php the_excerpt(__('Read more'));?>
				</div>
				<div class="post-meta">
					<p class="post_comments"><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>
					<p class="post_categories"><?php the_category(', ') ?></p>
					<!-- <p class="post_tags"><?php if (the_tags('', ', ', ' ')); ?></p> -->
				</div><!--.postMeta-->
			</div><!--.post-single-->
		<?php endwhile; else: ?>
			<div class="no-results">
				<p><strong><?php _e('There has been an error.'); ?></strong></p>
				<p><?php _e('We apologize for any inconvenience, please hit back on your browser or use the search form below.'); ?></p>
				<?php get_search_form(); /* outputs the default Wordpress search form */ ?>
			</div><!--noResults-->
		<?php endif; ?>


		<div class="oldernewer">
			<p class="older"><?php next_posts_link('&laquo; Older Entries') ?></p>
			<p class="newer"><?php previous_posts_link('Newer Entries &raquo;') ?></p>
		</div><!--.oldernewer-->

	</div><!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>
