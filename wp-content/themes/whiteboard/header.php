<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<title><?php  if ( is_category() ) {
		echo 'Category Archive for &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_tag() ) {
		echo 'Tag Archive for &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
	} elseif ( is_archive() ) {
		wp_title(''); echo ' Archive | '; bloginfo( 'name' );
	} elseif ( is_search() ) {
		echo 'Search for &quot;'.wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
	} elseif ( is_home() ) {
		bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
	}  elseif ( is_404() ) {
		echo 'Error 404 Not Found | '; bloginfo( 'name' );
	} elseif ( is_single() ) {
		wp_title('');
	} else {
		echo wp_title(''); echo ' | '; bloginfo( 'name' );
	}
	?></title>
	<?php if( is_paged() || is_tag() ) { ?>
		<meta name="robots" content="noindex,nofollow">
	<?php } ?>
	<meta name="description" content="<?php wp_title(''); echo ' | '; bloginfo( 'description' ); ?>" />
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<meta name="viewport" content="width=device-width; initial-scale=1"/><?php /* Add "maximum-scale=1" to fix the Mobile Safari auto-zoom bug on orientation changes, but keep in mind that it will disable user-zooming completely. Bad for accessibility. */ ?>
	<link rel="icon" href="<?php bloginfo('template_url'); ?>/whiteboard_favicon.ico" type="image/x-icon" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'atom_url' ); ?>" />
	<?php wp_enqueue_script("jquery"); /* Loads jQuery if it hasn't been loaded already */ ?>
	<?php /* The HTML5 Shim is required for older browsers, mainly older versions IE */ ?>
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<?php wp_head(); ?> <?php /* this is used by many Wordpress features and for plugins to work proporly */ ?>
		<?php /* Remove the Less Framework CSS line to not include the CSS Reset, basic styles/positioning, and Less Framework itself */?>
		<link href="http://fonts.googleapis.com/css?family=Bubblegum+Sans" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/lessframework.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/theme.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/jquery-1.7.2.min.js" ></script>
		<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/theme.js" ></script>

		<link href="<?php bloginfo( 'template_url' ); ?>/favicon.png" type="image/x-icon" rel="icon" />
		<link href="<?php bloginfo( 'template_url' ); ?>/favicon.png" type="image/x-icon" rel="shortcut icon" />


		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/lessframework.css" />

		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
	</head>

	<body <?php body_class(); ?>>
		<div class="none">
			<p><a href="#content"><?php _e('Skip to Content'); ?></a></p><?php /* used for accessibility, particularly for screen reader applications */ ?>
		</div><!--.none-->
		<div id="main"><!-- this encompasses the entire Web site -->
			<!--<div id="header"><header>-->
			<!--<div class="container">-->
			<div id="title" class="topbar">
				<?php if( is_front_page() || is_home() || is_404() ) { ?>
				<span id="logo" style="display: none;"><a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a></span>
				<span id="tagline" style="display: none;"><?php bloginfo('description'); ?></span>
				<?php } else { ?>
				<span id="logo" style="display: none;"><a href="<?php bloginfo('url'); ?>/" title="<?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a></span>
				<h3 id="tagline" style="display: none;"><?php bloginfo('description'); ?></h3>
				<?php } ?>
				<div id="logo" class="f1"><a href="/"></a><img style="display: none;" src="/wp-content/themes/whiteboard/images/logo_img.png"></div>

				<div class="top_searchbar">
					<form accept-charset="utf-8" method="post" id="PrintableSearchForm" action="http://printablesfree.com/printables/search">
						<div style="display:none;"><input type="hidden" value="POST" name="_method"></div>
						<input type="text" class="required" default_value="Search printables..." value="Search printables..." name="keywords">
						<input type="submit" value=" " class="search_btn_top">
					</form>
					<a href="http://printablesfree.com/tags/tagcloud" style="color: #ffffff !important;">Searches &amp; Tags</a>
				</div>
				<div class="cl"></div>
			</div><!--#title-->
<!--
			<?php
				// Check to see if the header image has been removed
				$header_image = get_header_image();
				if ( ! empty( $header_image ) ) :
			?>
				<?php
					// The header image
					// Check if this is a post or page, if it has a thumbnail, and if it's a big one
					if ( is_singular() &&
							has_post_thumbnail( $post->ID ) &&
							( /* $src, $width, $height */ $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( HEADER_IMAGE_WIDTH, HEADER_IMAGE_WIDTH ) ) ) &&
							$image[1] >= HEADER_IMAGE_WIDTH ) :
						// Houston, we have a new header image!
						echo get_the_post_thumbnail( $post->ID, 'post-thumbnail' );
					else : ?>
					<div id="header-image" class="container">
						<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="<?php bloginfo('name'); ?>" />
					</div><!--#header-image-->
				<?php endif; // end check for featured image or standard header ?>
			<?php endif; // end check for removed header image ?>

<!---			<div id="nav-primary" class="nav"><nav>
				<?php if ( is_user_logged_in() ) {
				     wp_nav_menu( array( 'theme_location' => 'logged-in-menu' ) ); /* if the visitor is logged in, this primary navigation will be displayed */
				} else {
				     wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); /* if the visitor is NOT logged in, this primary navigation will be displayed. if a single menu should be displayed for both conditions, set the same menues to be displayed under both conditions through the Wordpress backend */
				} ?>
			</nav></div><!--#nav-primary -->
			<!--
			<?php if ( ! dynamic_sidebar( 'Header' ) ) : ?><!-- Wigitized Header --><?php endif ?>
			<!--</div>--><!--.container-->
			<!--</header></div>--><!--#header-->
			<div class="container">
				<div id="subheader_social" style="margin-top: 10px;">
					<div class="fb-like" data-href="http://printablesfree.com/" data-send="true" data-width="320" data-show-faces="false"></div>
					<a href="https://twitter.com/PrintablesFree" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @twitter</a>
					<a href="http://pinterest.com/printablefree/"><img src="http://passets-lt.pinterest.com/images/about/buttons/follow-me-on-pinterest-button.png" width="169" height="28" alt="Follow Me on Pinterest" /></a>
					<div class="g-plusone" data-annotation="none" style="margin-bottom: 5px;" data-href="http://printablesfree.com/"></div>
					<script type="text/javascript">
						(function() {
							var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
							po.src = 'https://apis.google.com/js/plusone.js';
							var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
						})();
					</script>

				</div>