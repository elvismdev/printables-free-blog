<div id="categories_menu">
	<div id="categories_menu_header">
		<a href="/">Categories</a>
	</div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/invitations-">
			<img src="http://printablesfree.com/assets/image/category/80/small/"> Printable Invitations</a>
		</div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/cards">
			<img src="http://printablesfree.com/assets/image/category/83/small/"> Printable Cards</a>
		</div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/calendars-">
			<img src="http://printablesfree.com/assets/image/category/85/small/"> Printable Calendars</a>
		</div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/games">
                <img src="http://printablesfree.com/assets/image/category/89/small/"> Printable Games</a>
        </div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/worksheets">
                <img src="http://printablesfree.com/assets/image/category/84/small/"> Printable Worksheets</a>
        </div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/coupons">
			<img src="http://printablesfree.com/assets/image/category/109/small/"> Printable Coupons</a>
		</div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/quotes-and-sayings">
                    <img src="http://printablesfree.com/assets/image/category/92/small/"> Quotes and Sayings</a>
        </div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/lessons">
                <img src="http://printablesfree.com/assets/image/category/86/small/"> Printable Lessons</a>
        </div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/crafts">
                <img src="http://printablesfree.com/assets/image/category/90/small/"> Printable Crafts</a>
        </div>
        <div class="category_menu_item">
            <a href="http://printablesfree.com/categories/music">
                <img src="http://printablesfree.com/assets/image/category/88/small/"> Printable Music</a>
        </div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/coloring-pages">
			<img src="http://printablesfree.com/assets/image/category/87/small/"> Printable Coloring Pages</a>
		</div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/activities">
			<img src="http://printablesfree.com/assets/image/category/91/small/"> Printable Activities</a>
		</div>
        <div class="category_menu_item">
		<a href="http://printablesfree.com/categories/pictures">
			<img src="http://printablesfree.com/assets/image/category/93/small/"> Printable Pictures</a>
		</div>
		<div class="category_menu_item">
		<a href="http://printablesfree.com/categories/new">
		<img src="http://printablesfree.com/assets/image/category/new/small"> New Printables</a>
	</div>
</div>

<div id="sidebar">
		<?php if ( ! dynamic_sidebar( 'Sidebar' )) : ?>
	<ul>	
		<li id="sidebar-search" class="widget">
			<h3><?php _e('Search'); ?></h3>
			<?php get_search_form(); /* outputs the default Wordpress search form */ ?>
		</li>
		
		<li id="sidebar-nav" class="widget menu">
			<h3><?php _e('Navigation'); ?></h3>
			<ul>
				<?php wp_nav_menu( array( 'theme_location' => 'sidebar-menu' ) ); /* editable within the Wordpress backend */ ?>
			</ul>
		</li>
		
		<li id="sidebar-archives" class="widget">
			<h3><?php _e('Archives') ?></h3>
			<ul>
				<?php wp_get_archives( 'type=monthly' ); ?>
			</ul>
		</li>
	
		<li id="sidebar-meta" class="widget">
			<h3><?php _e('Meta') ?></h3>
			<ul>
				<?php wp_register(); ?>
				<li><?php wp_loginout(); ?></li>
				<?php wp_meta(); ?>
			</ul>
		</li>
	</ul>
		<?php endif; ?>
</div><!--sidebar-->
