<?php get_header(); ?>
<div id="content">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

<?php /*<script type="text/javascript"><!--
google_ad_client = "ca-pub-7784016101949285";

google_ad_slot = "7318340421";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
*/ ?>


			<article>
				<?php edit_post_link('<small>Edit this entry</small>','',''); ?>
				<h1><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				<!-- AddThis Button BEGIN -->
				<!-- <div class="addthis_toolbox addthis_default_style " style="float: right;">
					<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
					<a class="addthis_button_tweet"></a>
					<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
					<a class="addthis_button_pinterest_pinit"></a>
					<a class="addthis_counter addthis_pill_style"></a>
				</div> -->
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
				<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5090029a0cff6120"></script>
				<!-- AddThis Button END -->

				<p class="post_time_author"><?php _e('Posted on '); the_time('F j, Y'); _e(' at '); the_time() ?></p>

				<?php if ( has_post_thumbnail() ) { /* loades the post's featured thumbnail, requires Wordpress 3.0+ */ echo '<div class="featured-thumbnail">'; the_post_thumbnail(); echo '</div>'; } ?>
				<div class="post-content">
					<?php the_content(); ?>
					<?php wp_link_pages('before=<div class="pagination">&after=</div>'); ?>
				</div><!--.post-content-->
			</article>

			<div id="post-meta">
				<p class="post_comments"><?php comments_popup_link('No comments', 'One comment', '% comments', 'comments-link', 'Comments are closed'); ?> </p>
				<p class="post_categories"><?php the_category(', ') ?></p>
				<!-- <p class="post_tags"><?php //the_tags('', ', ', ' '); ?></p> -->
			</div><!--#post-meta-->

			<?php /* If a user fills out their bio info, it's included here */ ?>

		</div><!-- #post-## -->

		<div class="newer-older">
			<p class="older"><?php previous_post_link('%link', '&laquo; Previous post') ?>
			<p class="newer"><?php next_post_link('%link', 'Next Post &raquo;') ?></p>
		</div><!--.newer-older-->

<? /* Blog 728 Middle */ /*<script type="text/javascript"><!--
google_ad_client = "ca-pub-7784016101949285";

google_ad_slot = "1271806825";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
*/ ?>


	<?php // comments_template( '', true ); ?>

	<?php endwhile; /* end loop */ ?>

</div><!--#content-->
<?php get_sidebar(); ?>
<?php get_footer(); ?>