<?php include_once('sidebar-bg.gif');?>
	<div class="clear"></div>
	</div><!--.container-->

	<!--<div id="footer">

		<div class="footer_section footer_box" id="footer_left">
			<div id="footer_logo"></div>
			<div align="right">
				&copy; 2012 PrintablesFree.com
				<br>
				All Rights Reserved
			</div>
		</div>

		<div class="footer_section footer_box" id="footer_center">
			<div id="footer_links">
				<?php wp_nav_menu( array('theme_location' => 'footer-menu' )); /* editable within the Wordpress backend */ ?>
			</div>
			<div id="footer_social">
				<div id="footer_social_text">Follow Us</div>
				<a target="_blank" href="http://www.facebook.com/pages/Free-Printable-Online/199320310203045"><img src="/wp-content/themes/whiteboard/images/facebook_btn.png"></a>
				<a target="_blank" href="https://twitter.com/freeprintableon"><img src="/wp-content/themes/whiteboard/images/twitter_btn.png"></a>
				<a target="_blank" rel="publisher" href="https://plus.google.com/u/0/b/100071284082922656405/100071284082922656405/"><img src="/wp-content/themes/whiteboard/images/gplus_btn.png"></a>
				<a target="_blank" href="http://pinterest.com/freeprintableon/"><img src="/wp-content/themes/whiteboard/images/pinterest_btn.png"></a>
			</div>
		</div>

		<div class="footer_section" id="footer_right">
			<div class="newsletter_heading">Weekly Newsletter!</div>
			<div class="newsletter_subheading">100% Privacy Guaranteed</div>
			<div class="newsletter_box">
				<form accept-charset="utf-8" method="post" id="NewsletterEmailsSubscribeForm" action="http://printablesfree.com/newsletter_emails/subscribe"><div style="display:none;"><input type="hidden" value="POST" name="_method"></div>		<input type="text" class="required email" default_value="Enter your email address..." value="Enter your email address..." name="email">
					<div class="submit"><input type="submit" value="Sign me UP!"></div></form></div>
		</div>

	</div>-->



		<div class="footer_bg">
        	<div class="footer">

				<div class="footer_box">
                    <img src="/wp-content/themes/whiteboard/images/logo_fo.png"><br>

                    2013 Printables Free<br>
                    All Rights Reserved
                </div>

				<div class="footer_box">
                        <a href="#">Privacy</a> <a href="#">Terms</a> <a href="#">Contact Us</a> <a href="#">Printing Tips</a> <a href="#">Resources</a><br><br>

                  Follow Us
                    <a href="http://www.facebook.com/pages/Printables-Free/170330729785821?skip_nax_wizard=true&success=1" target="_blank"><img src="/wp-content/themes/whiteboard/images/facebook_icon.png" alt=""></a>
                    <a href="https://twitter.com/PrintablesFree" target="_blank"><img src="/wp-content/themes/whiteboard/images/twitter_icon.png" alt=""></a>
			<a href="https://plus.google.com/u/0/117172359319436908932?rel=author"  target="_blank"><img src="/wp-content/themes/whiteboard/images/gplus_icon.png" alt=""></a>
                    <!--<a href="https://plus.google.com/u/0/b/100071284082922656405/100071284082922656405/" rel="publisher"  target="_blank"><img src="/wp-content/themes/whiteboard/images/gplus_icon.png" alt=""></a>-->
                    <a href="http://pinterest.com/printablefree/" target="_blank"><img src="/wp-content/themes/whiteboard/images/pinterest_icon.png" alt=""></a>

                </div>

              	<div class="footer_box" style="border-right:none;">

					<font style="font-size:30px; color:#92416D">Weekly Newsletter!</font>
                    <span class="subtitleweeklynews">100% Privacy Guaranteed</span>
            		<input name="" type="text" value="Enter your email address..."> <input name="" type="button" class="signmeup_btn">

                </div>

                <div class="cl"></div>

            </div>

        </div>





</div><!--#main-->
<?php wp_footer(); /* this is used by many Wordpress features and plugins to work proporly */ ?>

	<a href="http://printablesfree.com/" class="blog_site_btn">
		<img src="<?php bloginfo( 'template_url' ); ?>/images/site.png" />
	</a>

	<div id="newsletter_button_fixed"></div>
	<div id="newsletter_box_fixed">
		<div class="newsletter_heading">Weekly Newsletter!</div>
		<br>
		<div class="newsletter_subheading">100% Privacy Guaranteed</div>
		<br>
		<div class="newsletter_box">
			<form accept-charset="utf-8" method="post" id="NewsletterEmailsSubscribeForm" action="http://printablesfree.com/newsletter_emails/subscribe">
				<div style="display:none;"><input type="hidden" value="POST" name="_method"></div>
				<input type="text" class="required email" default_value="Enter your email address..." value="Enter your email address..." name="email" />
				<div class="submit"><input type="submit" value="Sign me UP!" /></div>
			</form>
		</div>
	</div>

</body>
</html>